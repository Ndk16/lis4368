
# LIS4368

## Nicholas Koester

### Project 1 Requirements:

*Three parts:*

1. Review the code on index.jsp
2. Change title, navigation links, and header tags appropriately
3. Add form controls to match attributes of customerentity

#### README.md file should include the following items:

* Screenshot of main home page
* Screenshot of Failed validation of Project 1
* Screenshot of Passed validation of Project 1



#### Project Screenshots:

*LIS4368 Portal (Main/Splash Page)*:

![Main Page](img/home.png)

*Failed Validation*:

![Failed validation of project 1](img/failed.png)

*Passed Validation*:

![Passed validation of project 1](img/passed.png)





