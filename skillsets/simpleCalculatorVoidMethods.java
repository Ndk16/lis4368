import java.util.Scanner;
import java.lang.Math;


public class simpleCalculatorVoidMethods {
    public static void main(String[] args) {
        String operation = "";
        int num1, num2;

        Scanner input = new Scanner(System.in);

        System.out.print("Enter mathematical operation (a=add, s=subtract, m=multiply, d=divide, p=power): ");
        operation = input.next().toLowerCase();

        while(!operation.equals("a") && !operation.equals("s") && !operation.equals("m") && !operation.equals("d") && !operation.equals("p"))
        {
            System.out.print("Incorrect operation. Please enter correct operation: ");
            operation = input.next().toLowerCase();
        }

        System.out.print("Please enter first number: ");
        while(!input.hasNextInt()){
            System.out.println("Not valid integer!");
            input.next();
            System.out.print("Please try again. Enter first number: ");
        }
        num1 = input.nextInt();

        System.out.print("Please enter second number: ");
        while(!input.hasNextInt()){
            System.out.println("Not valid integer!");
            input.next();
            System.out.print("Please try again. Enter second number: ");
        }
        num2 = input.nextInt();

        if(operation.equals("d") && num2 == 0){
            System.out.println("Cannot divide by zero!");
            //System.exit(0);
            return;
        }

        switch (operation)
        {
            case "a":
                add(num1, num2);
                return;
            case "s":
                sub(num1, num2);
                return;
            case "m":
                mult(num1, num2);
                return;
            case "d":
                div(num1, num2);
                return;
            case "p":
                pow(num1, num2);
                return;
        }
        return;
    }

    public static void add(int num1, int num2){
        int sum = num1 + num2;
        System.out.println("The sum of " + num1 + " and " + num2 + " equals " + sum);
        return;
    }
    public static void sub(int num1, int num2){
        int diff = num1 - num2;
        System.out.println("The subtraction of " + num1 + " and " + num2 + " equals " + diff);
        return;
    }
    public static void mult(int num1, int num2){
        int product = num1 * num2;
        System.out.println("The product of " + num1 + " and " + num2 + " equals " + product);
        return;
    }
    public static void div(int num1, int num2){
        double quotient = num1 / num2;
        System.out.println("The quotient of " + num1 + " and " + num2 + " equals " + quotient);
        return;
    }
    public static void pow(int num1, int num2){
        double answer = Math.pow(num1, num2);
        System.out.println("The power of " + num1 + " and " + num2 + " equals " + answer);
        return;
    }

}




