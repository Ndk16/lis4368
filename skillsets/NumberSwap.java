//Nicholas Koester
//LIS4368
//3-18-2021

import java.util.Scanner;


public class NumberSwap {
    public static void main(String[] args) {
        System.out.println("Program swaps two intergers.");
        System.out.println("Note: Program checks for integers and non-numeric values.");

        Scanner input = new Scanner(System.in);

        int num1, num2, temp;

        System.out.print("Please enter first number: ");
        while(!input.hasNextInt()){
            System.out.println("Not valid integer!");
            input.next();
            System.out.print("Please try again. Enter first number: ");
        }
        num1 = input.nextInt();

        System.out.print("Please enter second number: ");
        while(!input.hasNextInt()){
            System.out.println("Not valid integer!");
            input.next();
            System.out.print("Please try again. Enter second number: ");
        }
        num2 = input.nextInt();

        System.out.println("Before Swapping");
        System.out.println("num1 = " + num1);
        System.out.println("num2 = " + num2);

        temp = num1;
        num1 = num2;
        num2 = temp;

        System.out.println("After Swapping");
        System.out.println("num1 = " + num1);
        System.out.println("num2 = " + num2);
    }
}




