//Nicholas Koester
//LIS4368
//3-18-2021

import java.util.Scanner;

public class LargestThreeNumbers
{
    public static void main(String[] args)
    {
        System.out.println("Program evaluates largest of three intergers.");
        System.out.println("Note: Program checks for integers and non-numeric values.");

        Scanner input = new Scanner(System.in);

        int num1, num2, num3, temp;

        System.out.print("Please enter first number: ");
        while(!input.hasNextInt()){
            System.out.println("Not valid integer!");
            input.next();
            System.out.print("Please try again. Enter first number: ");
        }
        num1 = input.nextInt();

        System.out.print("Please enter second number: ");
        while(!input.hasNextInt()){
            System.out.println("Not valid integer!");
            input.next();
            System.out.print("Please try again. Enter second number: ");
        }
        num2 = input.nextInt();

        System.out.print("Please enter third number: ");
        while(!input.hasNextInt()){
            System.out.println("Not valid integer!");
            input.next();
            System.out.print("Please try again. Enter third number: ");
        }
        num3 = input.nextInt();

        if(num1 < num2 && num3 < num2){
            System.out.println("Second number is largest.");
        }
        else if(num2 < num1 && num3 < num1) {
            System.out.println("First number is largest.");
        }
        else if(num2 < num3 && num1 < num3) {
            System.out.println("Third number is largest.");
        }
        else if(num1 == num2 && num2 == num3){
            System.out.println("All three numbers are equal");
        }
        else if(num1 == num2 && num3 < num2){
            System.out.println("The first and second numbers are the equal and the largest.");
        }
        else if(num2 == num3 && num1 < num2){
            System.out.println("The second and third numbers are the equal and the largest.");
        }
        else if(num1 == num3 && num2 < num3){
            System.out.println("The first and third numbers are the equal and the largest.");
        }
    }
}