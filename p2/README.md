> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Nicholas Koester

### Project 2 Requirements:

*Three parts:*

1. Complete the JSP/Servlets web application using the MVC framework
2. Add create, read, update and delete(CRUD) functionality
3. [Optional] Search (SCRUD) option

#### README.md file should include the following items:

* Screenshot of Validation forms of P2

#### Assignment Screenshots:

*Valid User Form Entry(customerform.jsp)*:

![Customer form](img/customerForm_1.png)

*PassedValidation(thanks.jsp)*:

![Passed customer validation](img/passedValidation_2.png)

*Display Data(customers.jsp)*:

![Display customer data](img/displayData_3.png)

*Modify Form(modify.jsp)*:

![Update customer form](img/updateCustomer_4.png)


*Modified Data(customers.jsp)*:

![Modified customer data](img/modifiedData_5.png)


*Delete Warning(customers.jsp)*:

![Delete warning](img/deleteWarning_6.png)



