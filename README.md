> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4368 - Advanced Web Application Development 

## Nicholas Koester

### LIS4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials
      (Bitbucketstationlocations and myteamquotes)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - MySQL Installation
    - Compiling and Using Servlets
    - Database Connectivity Using Servlets

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Entity Relationship Diagram (ERD)
    - Include data (at least 10 records each table)
    - Provide Bitbucket read-only access to repo (Language SQL) *must* include README.md, using Markdown syntax, and include links to *all* of the following files (from README.md):
	- docs folder: a3.mwb and a3.sql
	- img folder: a3.png (export a3.mwb file as a3.png)
	- README.md (*MUST* display a3.png ERD)
    - Blackboard Links: Bitbucket rep

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Basic server-side validation 
    - Pass and failed validation of a servlet form
    - Skillset 10 Java: Count Characters
    - Skillset 11 Java: File Write/Read Count Words
    - Skillset 12 Java: ASCII App

5. [A5 README.md](a5/README.md "My A5 README.md file")

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Main home page
    - Failed Validation check
    - Passed Validation check

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - JSP/Servlets web application using the MVC framework
    - Add create, read, update and delete(CRUD) functionality
    - Screenshots of various forms of CRUD functionality