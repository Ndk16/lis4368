
# LIS4368

## Nicholas Koester

### Assignment 1 Requirements:

*Two parts:*

1. Multiple screenshots of multiple web pages
2. Screenshot of query results 

#### README.md file should include the following items:

* http://localhost:9999/hello/ running
* http://localhost:9999/hello/HelloHome.html running
* http://localhost:9999/hello/sayhello running 
* http://localhost:9999/hello/querybook.html running 
* http://localhost:9999/hello/query?author=Tan+Ah+Teck search result running

#### Assignment Screenshots:

*Screenshot of running http://localhost:9999/hello*:

![http://localhost:9999/hello Screenshot](img/directory.png)

*Screenshot of running http://localhost:9999/hello/index.html*:

![http://localhost:9999/hello/index.html Screenshot](img/HelloHome.png)

*Screenshot of running http://localhost:9999/hello/sayhello*:

![http://localhost:9999/hello/sayhello Screenshot](img/sayhello.png)

*Screenshot of running http://localhost:9999/hello/querybook.html*:

![http://localhost:9999/hello/querybook.html Screenshot](img/querybook.png)

*Screenshot of query results*:

![Query results Screenshot](img/queryauthor.png)

*Screenshot of personal website*:

![Personal website Screenshot](img/website.png)



