import java.util.Scanner;

public class CountCharacters
{
	public static void main(String[] args) 
	{
		Scanner input = new Scanner(System.in);
		
		int letter = 0;
		int space = 0;
		int num = 0;
		int other = 0;
		String str = "";
		
		System.out.println("Please enter string: ");
		str = input.nextLine();
		
		char[] ch = str.toCharArray();
		
		for(int i = 0;i < str.length(); i++){
			if(Character.isLetter(ch[i])){
				letter++;
			}
			else if(Character.isDigit(ch[i])){
				num++;
			}
			else if(Character.isSpaceChar(ch[i])){
				space++;
			}
			else{
				other++;
			}
		}
		System.out.println("\nYour string: \"" + str +"\" has the following number and types of characters: ");
		System.out.println("Letter(s): " + letter);
		System.out.println("Space(s): " + space);
		System.out.println("Number(s): " + num);
		System.out.println("Other character(s): " + other);
	}
}