> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Nicholas Koester

### Assignment 4 Requirements:

*Three parts:*

1. Basic server-side validation 
2. Pass and failed validation of a servlet form

#### README.md file should include the following items:

* Screenshot of Validation forms of A4

#### Assignment Screenshots:

*Failed Validation:*:

![A4 failed validation](img/fValidation.png)

*Passed Validation:*:

![A4 passed validation](img/pValidation.png)


