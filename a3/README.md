> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Nicholas Koester

### Assignment 3 Requirements:

*Four parts:*

1. Entity Relationship Diagram (ERD)
2. Include data (at least 10 records each table)
3. Provide Bitbucket read-only access to repo (Language SQL) *must* include README.md, using Markdown syntax, and include links to *all* of the following files (from README.md):
	- docs folder: a3.mwb and a3.sql
	- img folder: a3.png (export a3.mwb file as a3.png)
	- README.md (*MUST* display a3.png ERD)
4. Blackboard Links: Bitbucket rep

#### README.md file should include the following items:

* Screenshot of ERD that links to the image:

#### Assignment Screenshots:

*Screenshot of A3 ERD:*:

![A3 Screenshot](img/a3_ERD.png)

*Screenshot of A3 webpage:*:

![A3 Webpage](img/a3_webpage.png)


*A3 docs: a3.mwb and a3.sql*:

[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format")

[A3 SQL File](docs/a3.sql "A3 SQL Script")